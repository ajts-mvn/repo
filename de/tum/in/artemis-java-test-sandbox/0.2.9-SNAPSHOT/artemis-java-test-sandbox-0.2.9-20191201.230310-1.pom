<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>de.tum.in</groupId>
	<artifactId>artemis-java-test-sandbox</artifactId>
	<version>0.2.9-SNAPSHOT</version>
	<name>Artemis Java Test Sandbox</name>
	<description>JUnit 5 Extension API for Artemis Java Testing</description>
	<inceptionYear>2019</inceptionYear>
	<licenses>
		<license>
			<name>MIT</name>
			<url>https://opensource.org/licenses/MIT</url>
		</license>
	</licenses>
	<developers>
		<developer>
			<name>Christian Femers</name>
			<email>christian.femers@tum.de</email>
			<organization>Technical University of Munich</organization>
			<organizationUrl>https://www.tum.de</organizationUrl>
		</developer>
	</developers>

	<properties>
		<maven.compiler.source>11</maven.compiler.source>
		<maven.compiler.target>11</maven.compiler.target>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
	</properties>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>3.2.0</version>
				<configuration>
					<archive>
						<manifestEntries>
							<Sealed>true</Sealed>
						</manifestEntries>
					</archive>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>3.1.1</version>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>jar</goal>
						</goals>
						<configuration>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>3.0.0-M4</version>
				<!-- configuration> <forkCount>1C</forkCount> </configuration -->
			</plugin>
			<!-- plugin> <groupId>org.apache.maven.plugins</groupId> <artifactId>maven-gpg-plugin</artifactId>
				<version>1.6</version> <executions> <execution> <id>sign-artifacts</id> <phase>verify</phase>
				<goals> <goal>sign</goal> </goals> </execution> </executions> </plugin -->
		</plugins>
	</build>
	<distributionManagement>
		<repository>
			<id>internal</id>
			<url>file://${project.build.directory}/mvn-repo</url>
		</repository>
	</distributionManagement>

	<dependencies>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-api</artifactId>
			<version>5.5.2</version>
		</dependency>
		<dependency>
			<groupId>org.junit.platform</groupId>
			<artifactId>junit-platform-launcher</artifactId>
			<version>1.5.2</version>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-engine</artifactId>
			<version>5.5.2</version>
		</dependency>
		<dependency>
			<groupId>org.junit.vintage</groupId>
			<artifactId>junit-vintage-engine</artifactId>
			<version>5.5.2</version>
		</dependency>
		<!-- This is included for better String comparison error messages -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.12</version>
		</dependency>
		<dependency>
			<groupId>org.hamcrest</groupId>
			<artifactId>hamcrest</artifactId>
			<version>2.2</version>
		</dependency>
		<dependency>
			<groupId>org.assertj</groupId>
			<artifactId>assertj-core</artifactId>
			<version>3.14.0</version>
		</dependency>
		<!-- For testing we use a test framework testing framework -->
		<dependency>
			<groupId>org.junit.platform</groupId>
			<artifactId>junit-platform-testkit</artifactId>
			<scope>test</scope>
			<version>1.5.2</version>
		</dependency>
		<!-- For testing the property based testing framework jqwick is supported -->
		<dependency>
			<groupId>net.jqwik</groupId>
			<artifactId>jqwik</artifactId>
			<version>1.2.1</version>
			<optional>true</optional>
			<scope>provided</scope>
		</dependency>
	</dependencies>
</project>