<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>de.tum.in</groupId>
	<artifactId>artemis-java-test-sandbox</artifactId>
	<version>0.5.0-SNAPSHOT</version>
	<name>Artemis Java Test Sandbox</name>
	<description>JUnit 5 Extension API for secure Artemis Java Testing</description>
	<url>https://github.com/MaisiKoleni/artemis-java-test-sandbox</url>
	<inceptionYear>2019</inceptionYear>
	<licenses>
		<license>
			<name>MIT</name>
			<url>https://opensource.org/licenses/MIT</url>
			<comments>See https://github.com/MaisiKoleni/artemis-java-test-sandbox/blob/master/LICENSE</comments>
		</license>
	</licenses>
	<developers>
		<developer>
			<name>Christian Femers</name>
			<email>christian.femers@tum.de</email>
			<organization>Technical University of Munich</organization>
			<organizationUrl>https://www.tum.de</organizationUrl>
		</developer>
	</developers>

	<scm>
		<url>https://github.com/MaisiKoleni/artemis-java-test-sandbox</url>
		<connection>scm:git:https://github.com/MaisiKoleni/artemis-java-test-sandbox.git</connection>
		<developerConnection>scm:git:https://github.com/MaisiKoleni/artemis-java-test-sandbox.git</developerConnection>
	</scm>

	<properties>
		<maven.compiler.source>11</maven.compiler.source>
		<maven.compiler.target>11</maven.compiler.target>
		<junit-jupiter-version>5.6.1</junit-jupiter-version>
		<junit-platform-version>1.6.1</junit-platform-version>
		<jqwik-version>1.2.6</jqwik-version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
	</properties>

	<dependencies>
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>1.2.3</version>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-api</artifactId>
			<version>${junit-jupiter-version}</version>
		</dependency>
		<dependency>
			<groupId>org.junit.platform</groupId>
			<artifactId>junit-platform-launcher</artifactId>
			<version>${junit-platform-version}</version>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-engine</artifactId>
			<version>${junit-jupiter-version}</version>
		</dependency>
		<dependency>
			<groupId>org.junit.vintage</groupId>
			<artifactId>junit-vintage-engine</artifactId>
			<version>${junit-jupiter-version}</version>
			<scope>provided</scope>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>org.hamcrest</groupId>
			<artifactId>hamcrest</artifactId>
			<version>2.2</version>
		</dependency>
		<dependency>
			<groupId>org.assertj</groupId>
			<artifactId>assertj-core</artifactId>
			<version>3.15.0</version>
		</dependency>
		<!-- For testing we use a test framework testing framework -->
		<dependency>
			<groupId>org.junit.platform</groupId>
			<artifactId>junit-platform-testkit</artifactId>
			<scope>test</scope>
			<version>${junit-platform-version}</version>
		</dependency>
		<!-- For testing the property based testing framework jqwik is supported
			(but it is not a required dependency) -->
		<dependency>
			<groupId>net.jqwik</groupId>
			<artifactId>jqwik</artifactId>
			<version>${jqwik-version}</version>
			<scope>provided</scope>
			<optional>true</optional>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>3.2.0</version>
				<configuration>
					<archive>
						<manifestEntries>
							<Sealed>true</Sealed>
						</manifestEntries>
					</archive>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>3.2.0</version>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>3.2.1</version>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>3.0.0-M4</version>
				<configuration>
					<argLine>-Dfile.encoding=UTF-8</argLine>
					<runOrder>alphabetical</runOrder>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-deploy-plugin</artifactId>
				<version>3.0.0-M1</version>
			</plugin>
			<!-- plugin> <groupId>org.apache.maven.plugins</groupId> <artifactId>maven-gpg-plugin</artifactId>
				<version>1.6</version> <executions> <execution> <id>sign-artifacts</id> <phase>verify</phase>
				<goals> <goal>sign</goal> </goals> </execution> </executions> </plugin -->
		</plugins>
	</build>

	<profiles>
		<!-- This is only for GitHub -->
		<profile>
			<id>github</id>
			<distributionManagement>
				<repository>
					<id>github</id>
					<name>GitHub MaisiKoleni Apache Maven Packages</name>
					<url>https://maven.pkg.github.com/MaisiKoleni/artemis-java-test-sandbox</url>
				</repository>
				<!-- GitHub cannot handle snapshots correctly now, so we do nothing -->
				<snapshotRepository>
					<id>internal</id>
					<url>file://${project.build.directory}/mvn-repo</url>
				</snapshotRepository>
			</distributionManagement>
		</profile>
		<!-- This is only for GitLab as a deploy workaround -->
		<profile>
			<id>gitlab</id>
			<distributionManagement>
				<repository>
					<id>internal</id>
					<url>file://${project.build.directory}/mvn-repo</url>
				</repository>
			</distributionManagement>
		</profile>
	</profiles>
</project>